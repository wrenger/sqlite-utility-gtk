import os
import sqlite3

class DBError(Exception):
    pass

connection = None

def connect(filename):
    if is_connected():
        close()

    global connection
    try:
        connection = sqlite3.connect(filename)
    except Exception as e:
        connection = None
        raise DBError(str(e))


def close():
    if is_connected():
        global connection
        connection.close()
        connection = None


def is_connected():
    return connection is not None


def execute(command):
    if not is_connected():
        raise DBError('No Database Connection established!')

    try:
        c = connection.cursor()
        c.execute(command)
        result = None
        if c.description:
            columns = [col[0] for col in c.description]
            result = [columns] + c.fetchall()
        rowcount = c.rowcount
        c.close()
        connection.commit()

        if result:
            return result
        else:
            return rowcount
    except Exception as e:
        raise DBError(str(e))


def tables():
    try:
        c = connection.cursor()
        c.execute('SELECT name FROM sqlite_master WHERE type = "table"')
        return [r[0] for r in c.fetchall()]
    except Exception as e:
        raise DBError(str(e))

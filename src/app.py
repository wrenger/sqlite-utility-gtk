import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio

from appio import RESOURCE_PATH


if __name__ == "__main__":
    resource = Gio.Resource.load(RESOURCE_PATH)
    resource._register()


from window import MainWindow
from about import AboutDialog

class Application(Gtk.Application):
    """
    The application class exists during the whole lifetime of this program.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="com.larswrenger.sqlite-utility",
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **kwargs)
        self.window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        builder = Gtk.Builder.new_from_resource(
            '/com/larswrenger/sqlite-utility/menu.xml')
        self.set_app_menu(builder.get_object("menu"))

    def do_activate(self):
        if not self.window:
            self.window = MainWindow(application=self)

        self.window.present()

    def do_command_line(self, command_line):
        self.activate()
        return 0

    def on_about(self, action, param):
        dialog = AboutDialog(transient_for=self.window)
        dialog.run()
        dialog.destroy()

    def on_quit(self, action, param):
        self.quit()


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)

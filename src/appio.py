import os
import sys

APP_ROOT = os.path.abspath(os.path.dirname(__file__))
(APP_ROOT, _) = os.path.split(APP_ROOT)

RESOURCE_PATH = os.path.join(APP_ROOT, 'data',
                             'sqlite-utility.gresource')


def shorten_path(filename):
    filename = os.path.normpath(os.path.abspath(filename))
    home = os.path.expanduser('~')
    if (os.path.commonprefix([filename, home]) == home):
        return os.path.join('~', os.path.relpath(filename, start=home))
    else:
        return filename

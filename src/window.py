from os import path
from gi.repository import Gtk, Gio

from appio import shorten_path
import database as db


def db_open_dialog(window):
    dialog = Gtk.FileChooserDialog("Open a Database", window,
                                   Gtk.FileChooserAction.OPEN,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

    filter_db = Gtk.FileFilter()
    filter_db.set_name('Databases')
    filter_db.add_mime_type('application/x-sqlite3')
    dialog.add_filter(filter_db)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Any files")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    filename = None
    if dialog.run() == Gtk.ResponseType.OK:
        filename = dialog.get_filename()

    dialog.destroy()
    return filename


def db_new_dialog(window):
    dialog = Gtk.FileChooserDialog("Create a new Database", window,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_NEW, Gtk.ResponseType.OK))

    filename = None
    if dialog.run() == Gtk.ResponseType.OK:
        filename = dialog.get_filename()

    dialog.destroy()
    return filename


def error_dialog(window, title, error):
    dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.ERROR,
                               Gtk.ButtonsType.CANCEL, title)
    dialog.format_secondary_text(str(error))
    dialog.run()
    dialog.destroy()


@Gtk.Template(resource_path='/com/larswrenger/sqlite-utility/window.ui')
class MainWindow(Gtk.ApplicationWindow):
    """
    Template class of the main Window.

    Uses the composite template method to extend this window by the
    Gtk.Builder XML representation.
    """
    __gtype_name__ = 'MainWindow'

    # -------------------------------------------------------------------------
    # important widgets
    # -------------------------------------------------------------------------

    header_bar = Gtk.Template.Child()
    command_field = Gtk.Template.Child()
    table_select = Gtk.Template.Child()
    result_table = Gtk.Template.Child()
    status = Gtk.Template.Child()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.table_select.set_model(Gtk.ListStore(str))
        self.status.set_text('No Database opened')

        action = Gio.SimpleAction.new("new", None)
        action.connect("activate", self.menu_new)
        self.add_action(action)
        action = Gio.SimpleAction.new("open", None)
        action.connect("activate", self.menu_open)
        self.add_action(action)
        action = Gio.SimpleAction.new("close", None)
        action.connect("activate", self.menu_close)
        self.add_action(action)

    # -------------------------------------------------------------------------
    # app menu window callbacks
    # -------------------------------------------------------------------------

    def menu_new(self, action, value):
        self.on_new(None)

    def menu_open(self, action, value):
        self.on_open(None)

    def menu_close(self, action, value):
        self.close()

    # -------------------------------------------------------------------------
    # widget signal handler
    # -------------------------------------------------------------------------

    @Gtk.Template.Callback()
    def on_open(self, btn):
        filename = db_open_dialog(self)
        if filename is not None:
            self.connect(filename)

    @Gtk.Template.Callback()
    def on_new(self, btn):
        filename = db_new_dialog(self)
        if filename is not None:
            self.connect(filename)

    @Gtk.Template.Callback()
    def on_schema(self, btn):
        self.table_select.set_property('active', -1)
        self.execute(
            'SELECT name, sql FROM sqlite_master WHERE type = "table"')

    @Gtk.Template.Callback()
    def on_execute(self, btn):
        self.table_select.set_property('active', -1)
        buf = self.command_field.get_buffer()
        command = buf.get_text(buf.get_start_iter(),
                               buf.get_end_iter(), False)
        self.execute(command)

    @Gtk.Template.Callback()
    def on_table_select(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            table = model[tree_iter][0]
            self.execute('select * from ' + table)

    # -------------------------------------------------------------------------
    # helper methods
    # -------------------------------------------------------------------------

    def connect(self, filename):
        try:
            db.connect(filename)
            self.display_tables()
            self.header_bar.set_subtitle(shorten_path(filename))
            self.status.set_text('Open ' + shorten_path(filename))
        except Exception as e:
            error_dialog(self, 'Database Error', e)

    def execute(self, command):
        if db.is_connected():
            try:
                result = db.execute(command)
                if result and isinstance(result, list):
                    self.display_result(result)
                else:
                    self.status.set_text('Update %d rows' % result)
            except db.DBError as e:
                error_dialog(self, 'Database Error', e)
        else:
            error_dialog(self, 'No Connection',
                         'Please open or create a Database.')

    def display_tables(self):
        model = self.table_select.get_model()
        model.clear()
        for table in db.tables():
            model.append([table])

    def display_result(self, result):
        self.result_table.set_model(None)
        for column in self.result_table.get_columns():
            self.result_table.remove_column(column)

        model = Gtk.ListStore(*([object] * len(result[0])))
        for row in result[1:]:
            model.append(list(row))
        self.result_table.set_model(model)

        for i, col in enumerate(result[0]):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(col, renderer, text=i)
            column.set_cell_data_func(
                renderer, MainWindow.object_cell_data_func, i)
            self.result_table.append_column(column)

        self.status.set_text('%d results' % (len(result) - 1))

    @staticmethod
    def object_cell_data_func(column, cell, model, iter, i):
        value = model[iter][i]
        if value is not None:
            cell.set_property('text', str(value))
            cell.set_property('foreground', None)
        else:
            cell.set_property('text', 'null')
            cell.set_property('foreground', 'grey')

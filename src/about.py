from gi.repository import Gtk


@Gtk.Template(resource_path='/com/larswrenger/sqlite-utility/about.ui')
class AboutDialog(Gtk.AboutDialog):
    """
    The about dialog.
    """

    __gtype_name__ = 'AboutDialog'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

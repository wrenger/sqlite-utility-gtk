import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

with open('requirements.txt', 'r') as fh:
    install_requires = fh.read()

setuptools.setup(
    name='com.larswrenger.sqlite-utility',
    version='0.0.1',
    author='Lars Wrenger',
    author_email='lars@wrenger.net',
    description='A Lightweight Tool for working with SQLite Databases.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/l4r0x/sqlite-utility-gtk',
    packages=setuptools.find_packages(include='src.*'),
    python_requires='>= 3.6',
    install_requires=install_requires,
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)

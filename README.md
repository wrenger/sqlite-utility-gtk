# SQLite Utility

A Lightweight Tool for working with SQLite Databases.

This is more a demo project for building [GTK+3](https://www.gtk.org/) GUIs
with [Glade](https://glade.gnome.org/) and
[PyGObject](https://pygobject.readthedocs.io/en/latest/).

## Setup Build Environment

First follow the steps on
[PyGObject Getting Started](https://pygobject.readthedocs.io/en/latest/getting_started.html)
to install GTK+3 and PyGObject.

### Setup Virtualenv & Requirements

```bash
python3 -m pip install virtualenv
# or
pip install virtualenv
```

> On some linux distributions there are additional packages for pip and venv
> that have to be installed using the default package manager (apt, yum...).

Setup `virtualenv` and the python dependencies.

```bash
# bash
# in the project directory
python3 -m venv venv

# install requirements
source venv/bin/activate
pip install wheel
pip install -r requirements.txt
```

```powershell
# powershell
python -m venv venv
# powershell: ExecutionPolicy has to be AllSigned/RemoteSigned
env\Scripts\activate
# install all dependencies in the local environment
pip install -r requirements.txt
```

> All python dependencies are stored in [requirements.txt](requirements.txt).

See https://virtualenv.pypa.io/en/stable/userguide/ for more information.

## Usage

**Start the Application**
```bash
# bash
source venv/bin/activate
# start flask & debugger
python src/app.py
```

```powershell
# powershell: ExecutionPolicy has to be AllSigned/RemoteSigned
env\Scripts\activate
# start flask & debugger
python src\app.py
```

# See Also

* [GTK+3](https://www.gtk.org/)
* [Glade](https://glade.gnome.org/)
* [PyGObject](https://pygobject.readthedocs.io/en/latest/)
